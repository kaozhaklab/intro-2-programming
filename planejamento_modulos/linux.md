HakLab :: Proposta pedagogica para adminstração de sistemas GNU/Linux
==

  * Introdução
	* Sistemas Operacionais
	* O Linux
	* Algumas características do Linux
	* Distribuições Linux
	* Software Livre
	* Instalação
	* Mercado Linux

  * Explicações Básicas
	* Hardware e Software 
	* Arquivos 
	* Extensão de arquivos 
	* Tamanho de arquivos 
	* Arquivo texto e binário
 
  * Diretório 
	* FHS - Filesystem Hierarchy Standard**
	* Diretório Raíz 
	* Diretório atual 
	* Diretório home 
	* Diretório Superior 
	* Diretório Anterior 
	* Caminho na estrutura de diretórios 
	* Exemplo de diretório 
	* Estrutura básica de diretórios do Sistema Linux 
	* Nomeando Arquivos e Diretórios 
	* Comandos 

  * Comandos Internos 
	* Comandos Externos 
	* Aviso de comando (Prompt) 
	* Interpretador de comandos 
	* Modo texto e modo gráfico**
	* Terminal Virtual (console) 
	* Login 
	* Logout 
	* Curingas 

  * Execução de programas
	* Executando um comando/programa 
	* path 
	* Tipos de Execução de comandos/programas 
	* Executando programas em seqüência 
	* ps 
	* top 
	* Controle de execução de processos 
	* Interrompendo a execução de um processo 
	* Parando momentaneamente a execução de um processo 
	* jobs 
	* fg 
	* bg
	* kill 
	* killall 
	* killall5 
	* Sinais do Sistema 
	* Fechando um programa quando não se sabe como sair 
	* Eliminando caracteres estranhos 

  * Comandos para manipulação de diretório
	* ls 
	* cd 
	* pwd 
	* mkdir 
	* rmdir 

  * Comandos para manipulação de Arquivos
	* cat 
	* tac 
	* rm 
	* cp 
	* mv 

  * Comandos Diversos
	* clear 
	* date 
	* df 
	* ln 
	* du 
	* find 
	* free 
	* grep 
	* head 
	* nl
	* more 
	* less 
	* sort 
	* tail 
	* time 
	* touch 
	* uptime 
	* dmesg 
	* mesg 
	* echo 
	* su 
	* sync 
	* uname 
	* reboot 
	* shutdown 
	* wc 
	* seq 

  * Comandos de rede
	* who 
	* telnet 
	* finger 
	* ftp 
	* whoami 
	* dnsdomainname 
	* hostname 
	* talk 

  * Comandos para manipulação de contas
	* adduser 
	* addgroup 
	* passwd
	* gpasswd 
	* newgrp 
	* userdel 
	* groupdel 
	* sg 
	* Adicionando o usuário a um grupo extra 
	* chfn 
	* id 
	* logname 
	* users 
	* groups 

  * Permissões de acesso a arquivos e diretórios
	* Donos, Grupos e outros usuários 
	* Tipos de Permissões de Acesso 
	* Etapas para acesso a um arquivo/diretório 
	* Exemplos práticos de permissões de acesso 
	* Exemplo de acesso a um arquivo 
	* Exemplo de acesso a um diretório 
	* Permissões de Acesso Especiais 
	* A conta root 
	* chmod 
	* chgrp 
	* chown 
	* Modo de permissão octal 
	* umask 
	* Redirecionamentos e Pipe
	* \> 
	* \>\> 
	* \< 
	* \<\<
	* \| (pipe) 
	* Diferença entre o “\|” e o “\>” 
	* tee 

  * Compactação e descompactação zip, tar, gz, bz2, etc.
	* Compactando
	* Descompactando
	* Recursão em subdiretórios
	* Listagem de arquivos no pacote compactado

  * Edição de arquivos com o vi
	* inclusão
	* alteração
	* exclusão
	* repetição

  * Como obter ajuda no sistema
	* Páginas de Manual 
	* Info Pages 
	* Help on line 
	* help 
	* apropos/whatis 
	* locate 
	* which 
	* Documentos HOWTO’s 
	* Documentação de Programas 
	* FAQ 
	* Internet
	* Páginas Internet de Referência 
	* Listas de discussão 
