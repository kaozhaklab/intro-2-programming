HakLab :: Proposta pedagogica para organização de computadores

Organização de computadores:

  * 15h = 10 aulas

Objetivo:

  * Desenvolver competências relativas à correta montagem e configuração de computadores;
  * Capacitar o aluno a compreender e solucionar problemas em microcomputadores, realizando manutenção preventiva e corretiva, bem como identificação e correção de erros.

Planejamento de aulas:        

  * Aula 1
    * Computadores: princípio de funcionamento;
    * Computadores: histórico e evolução;
    * Introdução aos conceitos de hardware
  * Aula 2
    * Compreensão do computador (dispositivos de entrada e saída, armazenamento e processamento de dados);
    * Periféricos e seu uso, tipos e configurações;
    * Conceitos de bit, Byte e Hertz
  * Aula 3
    * Conceitos de Eletricidade/Eletrônica;
    * Carga eletrostática: eletrização e descarga
  * Aula 4
    * Componentes internos do computador (memória RAM, memória ROM, disco rígido);
    * Arquitetura interna, funcionamento e descrição;
    * Barramentos: funcionamento e desempenho
  * Aula 5
    * Componentes internos do computador (placa-mãe, processador, placas e slots de expansão, sistema de resfriamento);
    * Processadores Intel e AMD;
    * Padrões de placa-mãe
  * Aula 6
    * Exercicios
  * Aula 7
    * Componentes externos do computador (gabinete, fontes de alimentação, portas de saídas externas e cabos);
    * Padrões de fontes de alimentação
  * Aula 8
    * Componentes externos do computador (dispositivos de entrada e saída de dados, características do monitor);
    * Exercícios
  * Aula 9
    * Técnicas de montagem de computadores:
        * Apresentação das ferramentas para montagem;
        * Apresentação das técnicas de montagem (cuidados ao manusear o hardware, montagem correta dos componentes internos e externos do computador)
  * Aula 10
    * Instalando o sistema operacional Linux;
    * Exercícios
