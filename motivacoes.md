Computação na Educação Básica (primeiros 8 min):
https://www.youtube.com/watch?v=HOx6cpnsbpw

SBC na BNCC - Brasília/DF (primeiros 2 min e dos 8 aos 11):
https://www.youtube.com/watch?v=WtVc39DYxR8

Ensino de Computação na Educação Básica:
http://sbc.org.br/documentos-da-sbc/send/203-educacao-basica/1220-bncc-em-itinerario-informativo-computacao-2

Habilidades desenvolvidas pelo ensino de programação:
- Abstração e desenvolvimento de algoritmos
- Habilidades de coleta, interpretação e representação de dados
- Capacidade de modelagem e simulação
- Decomposição de problemas
- Pensamento computacional

Relação entre PC e programação

O pensamento computacional pode ser melhor entendido como uma habilidade intelectual fundamental para descrever e explicar problemas complexos e situações, podendo ser comparada à leitura, escrita, fala e aritmética, já que ambas possuem o mesmo propósito. Ou seja, o pensamento computacional é comparável a habilidades cognitivas básicas que são esperadas de uma pessoa média na sociedade moderna, Ursula Wolz, por exemplo, argumenta que a tarefa de programar é uma linguagem para representar ideias, pois você deve aprender a ler e escrever nesta linguagem para poder pensar nessa língua. Mitchel Resnick concorda, mas alega que o pensamento computacional é mais do que programação, da mesma forma que a alfabetização é mais do que escrever.

Alan Kay destaca que todos os seres humanos possuem uma capacidade inata de linguagem verbal, mas que o mesmo não pode ser dito para linguagem escrita, ciência e matemática dedutiva, pois estes não são comuns a todas as culturas ou sociedades. Portanto ele alega que os seres humanos não aprenderão o pensamento computacional da mesma forma que aprendem a falar, porém, ele também observa que um aspecto poderoso do pensamento computacional implica na capacidade de criar uma linguagem bem adaptada a um propósito pessoalmente relevante.


Elementos do PC:

Vários palestrantes afirmam que a depuração de sistemas é uma importante aplicação do pensamento computacional, pois é comum no mundo real, pessoas encontrarem sistemas com os quais não estão familiarizados e que não compreendem o seu funcionamento interno. Nessas situações é comum tentarmos encontrar um estado conhecido do sistema, baseado em experiências anteriores e encontros com sistemas semelhantes. Esse comportamento é um aspecto da modelagem do sistema desconhecido em nossas mentes, mesmo que não saibamos os algoritmos que estão dentro do sistema.

O teste refere-se a uma atividade empírica que fornece informações sobre o funcionamento de um artefato ou sistema de software. Considerando a impossibilidade de testar um sistema com todas as entradas possíveis, é importante estabelecer bons procedimentos de testes que abordem casos típicos, casos extremos e possíveis condições de falha.

O pensamento computacional fornece ferramentas intelectuais para ajudar no gerenciamento de informações, facilitando na mineração de dados e na recuperação de informações, possibilitando assim a obtenção de informações de variadas fontes de dados de forma que os resultados possam ser representados de uma maneira comum e que possam ser comunicados de diferentes maneiras.

Mitchel Resnick afirma que o paralelismo, enquanto capacidade de realizar tarefas simultaneamente, é um recurso muito natural que necessita das ideias computacionais de sequenciamento e paralelismo. São apresentados dois exemplos de ensino do conceito de paralelismo, sendo o primeiro, apresentado por Ursula Wolz, referente a sincronicidade entre uma animação e sua trilha sonora e o segundo, por Mitchel Resnick, referente a coreografia de um personagem seguindo o ritmo da música tocada.

A modelagem é um meio pelo qual se representa um sistema ou um processo para que se possa entender mais sobre ele e gerenciar sua complexidade. Nesta tarefa, o pensamento computacional ajuda no desenvolvimento efetivo de modelos complexos através do conhecimento de escala. A partir da alimentação do modelo com mais e mais dados, ele se sofisticará ao ponto de poder enfrentar testes que determinarão o comportamento real do sistema modelado. Yasmin Kafai destacou ainda a importância de compreender os modelos e suas limitações, citando que é comum as autoridades governamentais usarem modelos para realizar previsões que muitas vezes não são compreendidas pelas pessoas, portanto o pensamento computacional além de usar, experimentar e construir modelos, envolve também a criação de uma cultura de crítica ao modelo.
